const toggle = document.getElementById("togBtn");
const price1 = document.getElementById("price1");
const price2 = document.getElementById("price2");
const price3 = document.getElementById("price3");

window.onload = () => {
    toggle.checked = false;
}

toggle.onclick = () => {
    if (toggle.checked) {
        price1.innerHTML = "&dollar;19.99";
        price2.innerHTML = "&dollar;24.99";
        price3.innerHTML = "&dollar;39.99";

    } else {

        price1.innerHTML = "&dollar;199.99";
        price2.innerHTML = "&dollar;249.99";
        price3.innerHTML = "&dollar;399.99";
    }
}
